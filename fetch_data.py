from datetime import timedelta
from typing import Set, Tuple, List
from json import loads

from urllib3.exceptions import NewConnectionError, MaxRetryError

from astar import Node
from requests import get
import datetime


def stop_neighbors(stop: str) -> Set[Tuple[str, float]]:
    req = get("http://stop.gotdns.ch:8080/api/stop/name/" + stop)
    result = set()
    #if req.status_code == 200:
        # a = req.json()['nextList']

    a = loads("""[{"id":1,"name":"Konopnickiej","nextList":[{"nextStopId":2,"nextStopName":"Ludwinow","duration":2},{"nextStopId":3,"nextStopName":"AGH","duration":5},{"nextStopId":17,"nextStopName":"Plac","duration":3}]},
{"id":2,"name":"Ludwinow","nextList":[{"nextStopId":1,"nextStopName":"Konopnickiej","duration":2},{"nextStopId":3,"nextStopName":"AGH","duration":5}]},
{"id":3,"name":"AGH","nextList":[{"nextStopId":1,"nextStopName":"Konopnickiej","duration":5},{"nextStopId":2,"nextStopName":"Ludwinow","duration":5},{"nextStopId":4,"nextStopName":"Bronowice","duration":5}]},
{"id":4,"name":"Bronowice","nextList":[{"nextStopId":17,"nextStopName":"Plac","duration":4},{"nextStopId":3,"nextStopName":"AGH","duration":5}]},
{"id":17,"name":"Plac","nextList":[{"nextStopId":1,"nextStopName":"Konopnickiej","duration":3},{"nextStopId":4,"nextStopName":"Bronowice","duration":4}]}]""")

    for entry in a:
        if entry["name"] != stop:
            continue
        for next_stop in entry["nextList"]:
            result.add((next_stop['nextStopName'], next_stop['duration']))

    return result


def delay(node: Node) -> timedelta:
    try:
        req = get("http://somehostname:8080/api/bus/{0}/{1}/{2}/{3}".format(node.line, node.direction, node.stop, node.time))
        if req.status_code == 200:
            return timedelta(minutes=req.json()['delay'])
        return timedelta(minutes=0)
    except:
        return timedelta(minutes=0)


def departures(stop: str, timestamp: datetime) -> Set[Node]:
    """Receive the next few busses that stop here"""
    """
    req = get("http://stop.gotdns.ch:8080/api/stop/name/" + stop)
    result = set()
    if req.status_code == 200:
        for dep in req.json()['departures']:
            result.add(Node(stop=stop, time=dep['time'], line=dep['line'], direction=dep['direction']))
    return result
    """
    example = {"Bronowice":{12:(1,"Bronowice"),20:(1, "Konopnickiej"), 4:(2,"AGH"), 25:(2, "Plac")},
               "Konopnickiej": {0:(1, "Bronowice"), 5: (3 , "Plac"), 23:(3, "AGH")},
               "Ludwinow" : {2 :(1, "Bronowice"), 30 : (1, "Konopnickiej")},
               "AGH" : {0:(1, "Bronowice"), 25:(1,"Konopnickiej"), 20:(2, "Plac"), 0 : (3, "Plac")},
               "Plac" : {0 :(2, "AGH"), 20 : (3, "AGH")}
               }
    result =set()
    if stop not in example:
        return result
    for time, (line, dest) in example[stop].items():
        depart_time = timestamp
        if timestamp.minute > time:
            depart_time = depart_time + timedelta(hours=1)
        depart_time = depart_time.replace(minute=time)

        result.add(Node(stop, str(line), dest, depart_time))

    return result


def stops_on_line(line: str) -> List[str]:
    """
    req = get("http://somehostname:8080/api/line/" + line)
    if req.status_code == 200:
        return req.json()[busstops]
    """
    # todo: cache results of this function and refer to cache?
    a = []
    if str(line) == "1":
        a = ["Konopnickiej", "Ludwinow", "AGH", "Bronowice"]
    elif str(line) == "2":
        a = ["Plac", "Bronowice", "AGH"]
    elif str(line) == "3":
        a = ["AGH", "Konopnickiej", "Plac"]
    return a

