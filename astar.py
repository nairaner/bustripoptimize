from collections import defaultdict
from typing import Dict, Generator, Iterator, Callable, Set, Tuple

import attr

infinity = float("inf")


@attr.s(hash=True)
class Node:
    stop = attr.ib()  # str
    line = attr.ib()  # int
    direction = attr.ib()  # str
    time = attr.ib()  # datetime


def walk(parent: Dict[Node, Node], end: Node) -> Generator[Node, None, None]:
    current = end
    while current is not None:
        yield current
        current = parent.get(current, None)


def search(
        start: Node,
        is_end: Callable[[Node], bool],
        neighbors: Callable[[Node], Set[Tuple[Node, float]]],
        heuristic: Callable[[Node], float],
        max_checks_before_failure: int = -1,
) -> Iterator[Node]:
    to_check = {start}
    parent = dict()

    # the "g score" - total cost of the path so far
    path_cost = defaultdict(lambda: infinity)
    path_cost[start] = 0

    # the complete score, including heuristics and path cost.
    score = defaultdict(lambda: infinity)
    score[start] = heuristic(start)
    checks = 0

    while True:

        # since we may be working with an infinite graph, allow for failure if we take too long
        if max_checks_before_failure >= 0:
            checks += 1
            if checks > max_checks_before_failure:
                return []

        # if we run out of nodes to search, fail
        if len(to_check) == 0:
            return []

        current = min(to_check, key=lambda node: score[node])
        print(current)
        print(score[current])
        if is_end(current):
            return reversed(list(walk(parent, current)))
        to_check.remove(current)
        for (neighbor, distance) in neighbors(current):
            if path_cost[current] + distance < path_cost[neighbor]:
                parent[neighbor] = current
                path_cost[neighbor] = path_cost[current] + distance
                score[neighbor] = path_cost[neighbor] + heuristic(neighbor)
                to_check.add(neighbor)