import datetime


def parse_time(date_time_str: str) -> datetime:
    return datetime.datetime.strptime(date_time_str, '%Y%m%dT%H%M%S%z')


def export_time(date: datetime) -> str:
    return date.strftime('%Y%m%dT%H%M%S%z')
