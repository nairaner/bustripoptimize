
from datetime import timedelta
from flask_api import FlaskAPI
from flask_api.exceptions import NotFound, ParseError
from typing import Set, List, Tuple

from astar import search, Node
from timestamp import parse_time, export_time
from fetch_data import delay, departures, stop_neighbors, stops_on_line

app = FlaskAPI("Bus Trip Optimize")


@app.route("/route/<endpoints>/<timestamp>")
def get_route(endpoints: str, timestamp: str):
    if endpoints.count(".") != 1:
        raise NotFound(detail='Invalid route format')

    try:
        parsed_time = parse_time(timestamp)
    except ValueError:
        raise ParseError(detail='Invalid datetime format')

    origin, destination = endpoints.split(".")

    def heuristic(node: Node) -> float:
        return 0

    start = Node(origin, None, None, parsed_time)
    print("Searching for a route from %s to %s" % (origin, destination))
    route = list(search(start, lambda node: node.stop == destination, neighbors, heuristic, 64))
    print(route)

    if len(route) == 0:
        raise ParseError('No route could be found')

    return {
        "origin": origin,
        "destination": destination,
        "departure": export_time(route[1].time),
        "arrival": export_time(route[-1].time),
        "route": list(parse_route(route))
        }


def neighbors(node: Node) -> Set[Tuple[Node, float]]:
    neighbor_set = set()

    transfers = departures(node.stop, node.time)
    for trans in list(transfers):
        # we don't want to change from a line to the same line
        if trans.line == node.line and trans.direction == node.direction:
            transfers.remove(trans)

    # give a cost based on how long we'd wait for a change
    transfer_costs = map(lambda t: (t, (t.time - node.time + delay(t)).total_seconds() / 60), transfers)

    neighbor_set = neighbor_set.union(transfer_costs)

    stops = stops_on_line(node.line)

    try:
        backwards = node.direction == stops[0]
        next_stop = stops[stops.index(node.stop) + (-1 if backwards else 1)]

        minutes_to_next_stop = -1
        for nei, mints in stop_neighbors(node.stop):
            if nei == next_stop:
                minutes_to_next_stop = mints
        if minutes_to_next_stop >= 0:
            next_stop_time = node.time + timedelta(minutes=minutes_to_next_stop)
            neighbor_set.add((Node(next_stop, node.line, node.direction, next_stop_time), minutes_to_next_stop))
    except IndexError:
        pass  # there is no next stop

    return neighbor_set


def on_line(line: str, stop: str) -> bool:
    return stop in stops_on_line(line)


def parse_route(route: List[Node]):

    # aggregate the nodes in the route so each trip on a single bus is in one list
    # we skip the first element, because the route always starts with the user not on a bus yet
    aggregate_route = list()
    for node, prev_node in zip(route[1:], route[:-1]):
        if node.direction == prev_node.direction and node.line == prev_node.line:
            aggregate_route[-1].append(node)
        else:
            aggregate_route.append([node])

    # now analyze each trip individually
    for trip in aggregate_route:
        yield {
            "line": trip[0].line,
            "destination": trip[-1].stop,
            "departure":  export_time(trip[0].time),
            "arrival": export_time(trip[-1].time),
            "stops": len(trip) - 1
        }


@app.route("/route/<endpoints>/")
def get_next_route(endpoints: str) -> str:
    return get_route(endpoints, "now")


if __name__ == "__main__":
    app.run(port=8050)

